% !TeX spellcheck = nl_NL
% !TEX root = main.tex
% LTeX: language=nl
\chapter{Listings en code-fragmenten}
\label{sec:listings}
De commando's die gebruikt kunnen worden voor listings en codefragmenten zijn in een apart bestand geplaatst: |styleCode.tex|.
Er wordt gebruikt gemaakt van de package \hcode[style=lstyle]{list\\-ings}\footnote{\url{https://www.ctan.org/pkg/listings}}.

Er worden een aantal zogenoemde \squote{styles} gedefinieerd die in de onderstaande commando's gebruikt kunnen worden: |linenumbers| voegt regelnummers toe, |cstyle| maakt de code op als C-code, |cppstyle| als \Cpp{}-code, |lstyle| als \LaTeX\hyp{}code, |pystyle| als Python\hyp{}code, |mstyle| als MATLAB-code, |astyle| AVR\hyp{}assembler\hyp{}code, |amstyle| als MSP430\hyp{}assembler\hyp{}code, |legstyle| als LEG\hyp{}assembler\hyp{}code\footnote{%
    LEG\hyp{}assembler\hyp{}code wordt gebruikt in het boek \citetitle{Patterson2016} \cite{Patterson2016}.
}, |pinkystyle| als Pinky\hyp{}assembler\hyp{}code\footnote{%
    Pinky\hyp{}assembler\hyp{}code is een subset van de ARMv7 Thumb instructieset en is gedefinieerd in \cite{Broeders2022}.
} en |rstyle| als Rust\hyp{}code.

\section{Listings}
\label{sec:lstcommands}
De commando's |\floatlstinput| en |\lstinput| lezen code uit (een deel van) een bestand.
Deze commando's hebben vier parameters waarvan de eerste optioneel is.
Als eerste argument kunnen extra |key=value| paren voor het commando |\lstinputlisting| worden meegegeven, bijvoorbeeld: \hcode[style=lstyle]{style = cstyle, style = line\\-numbers, line\\-range = \{1-6, 13-27\}}.
Als tweede argument wordt de filenaam opgegeven.
Deze file moet in het directory |./progs| staan.
De derde parameter is een deel van het label, de labelnaam wordt {\lstllabel\hcode{lst:derde-ar\\-gu\\-ment}}.
De laatste parameter is de titel van de listing.
Het commando |\floatlstinput| heeft de voorkeur omdat \LaTeX{} dan zelf kan bepalen waar de listing wordt geplaatst en de listing dan niet halverwege afgebroken wordt.

De environments |floatlst| en |lst| kunnen gebruikt worden als de code niet uit een bestand moet worden gelezen, maar in de environment zelf wordt opgenomen.
Deze environments hebben drie parameters waarvan de eerste optioneel is.
Als eerste argument kunnen extra |key=value| paren voor het commando |\lstset| worden meegegeven, bijvoorbeeld: |style = cstyle, style = linenumbers|.
De tweede parameter is een deel van het label, de labelnaam wordt {\lstllabel\hcode{lst:tweede\\-argument}}.
De laatste parameter is de titel van de listing.
De environment |floatlst| heeft de voorkeur omdat \LaTeX{} dan zelf kan bepalen waar de listing wordt geplaatst en de listing dan niet halverwege afgebroken wordt.

Het commando |\lstinputfragment| kan gebruikt worden als je een codefragment uit een bestand wilt inlezen (zonder titel en zonder label).
Het fragment wordt op de plaats van het commando ingevuld.
Dit commando heeft twee parameters die gelijk zijn aan de eerste twee parameters van het commando |floatlstinput|.

De environment |lstfragment| kan gebruikt worden als een codefragment niet uit een bestand moet worden gelezen, maar in de environment zelf wordt opgenomen (zonder titel en zonder label).
Het fragment wordt op de plaats van het commando ingevuld.
Dit commando heeft één parameters die gelijk is aan de eerste parameter van de environment |floatlst|.

\section{\LaTeX-code in listings}
In een listing kun je ook \LaTeX-commando's gebruiken. Deze moeten dan wel omringd zijn door zogenoemde escape-karakters. Je moet zelf definiëren welke escape-karakters gebruikt worden m.b.v. de optie:\\ |escapeinside={BEGIN-ESCAPE-KARAKTERS}{END-ESCAPE-KARAKTERS}|.

Je kunt op deze manier tekst in een bepaalde stijl afdrukken of een voetnoot in je listing gebruiken, zie \cref{lst:latexincode}. 
\begin{lst}[style=lstyle]{latexincode}{Een voorbeeld van het gebruik van \LaTeX-code in een listing.}
\begin{lstfragment}[style=cstyle,escapeinside={(*}{*)}]{
	// Dit is een (*\textcolor{hrred}{belangrijk}*) voorbeeld.
	int main() {
		return 0(*\footnotemark*);
	}
\end{lstfragment}
\footnotetext{Sinds C99 mag dit \ccode{return}-statement ook weggelaten worden. De functie \ccode{main} geeft dan toch \ccode{0} terug.}
\end{lst}

Dit levert de volgende output:
\begin{lstfragment}[style=cstyle,escapeinside={(*}{*)}]
// Dit is een (*\textcolor{hrred}{belangrijk}*) voorbeeld.
int main() {
	return 0(*\footnotemark*);
}
\end{lstfragment}
\footnotetext{Sinds C99 mag dit \ccode{return}-statement ook weggelaten worden. De functie \ccode{main} geeft dan toch \ccode{0} terug.}

\section{Code-segmenten}
Inline code kan gedefinieerd worden met de commando's: |\code| voor taalonafhankelijke code, |\ccode| voor C-code, |\cppcode| voor \Cpp{}-code, |\lcode| voor \LaTeX\hyp{}code, |\pycode| voor Python\hyp{}code, |\mcode| voor MATLAB\hyp{}code, |\acode| voor AVR\hyp{}assembler\hyp{}code, |\amcode| voor MSP430\hyp{}assembler\hyp{}code, |\legcode| voor LEG\hyp{}assembler\hyp{}code, |\pinkycode| voor Pinky\hyp{}assembler\hyp{}code of |\rcode| voor Rust\hyp{}code.
Deze commando's hebben twee parameters waarvan de eerste optioneel is.
Als eerste argument kunnen extra |key=value| paren voor het commando |\lstinline| worden meegegeven.
De twee\-de parameter is de code die inline moet worden geplaatst.
Het karakter \code{\@} in het verplichte argument van het commando |\code| moet voorafgegaan worden door een |\|.

Het commando |\hcode| is een uitbreiding van het commando |\code|.
In de inline code kunnen mogelijke afbreeklocaties worden aangegeven met de tekencombinatie |\\-|.
Dit is nodig als een stuk inline code erg lange identifiers bevat.
Er zijn geen programmeertaal specifieke versies van gemaakt.
Dus als je de identifier \hcode{dit\\-Is\\-Een\\-Lange\\-Functie\\-naam} wilt laten afbreken in inline C-code dan kan dit als volgt in \LaTeX{}-code worden geschreven:
\begin{lstfragment}[style = lstyle]
\hcode[style=cstyle]{return dit\\-Is\\-Een\\-Lange\\-Functie\\-naam();}
\end{lstfragment}
Deze code levert het volgende, netjes afgebroken, resultaat: \hcode[style = cstyle]{return dit\\-Is\\-Een\\-Lange\\-Func\\-tie\\-naam();}.

Code kan nog eenvoudiger in lopende tekst worden opgenomen in één van de environments: |clstShortInline|, |cpplstShortInline|, |llstShortInline|, {\lstlenvironment\hcode{py\\-lst\\-Short\\-In\\-line}}, {\lstlenvironment\hcode{mlst\\-Short\\-In\\-line}}, 
|alstShortInline|, |amlstShortInline|, {\lstlenvironment\hcode{leg\\-lst\\-Short\\-In\\-line}}  of {\lstlenvironment\hcode{pinky\\-lst\\-Short\\-In\\-line}}.
Als in één van deze environments de syntax \code{\|code\|} wordt gebruikt, dan wordt deze code inline geplaatst.
Maar pas op: dit werkt niet in voetnoten, titels enz.

\section{Console in- en output}
De environment |coutput| kan gebruikt worden om console output weer te geven.
Binnen deze environment kan het commando |\cinput| gebruikt worden om console input weer te geven.
Console input wordt donkergroen en onderstreept weergegeven.
De console in- en output van een programma dat de som van twee integers bepaald, kun je bijvoorbeeld, als volgt in \LaTeX{} coderen:

\lstinputfragment[
    style=lstyle,
    rangebeginprefix= \%>,
    rangeendprefix=\%<,
    linerange={consolevoorbeeld-consolevoorbeeld},
]{../chap04.tex}

Bovenstaande \LaTeX{}-code geeft de volgende uitvoer:
%>consolevoorbeeld
\begin{coutput}
Geef een geheel getal: \cinput{13}
Geef nog een geheel getal: \cinput{7}
13 + 7 = 20
\end{coutput}
%<consolevoorbeeld

\section{Overige commando's die handig zijn bij documenten met programmeercode}
Het commando |\Cpp| kan gebruikt worden om de naam van de programmeertaal \Cpp{} netjes af te drukken.
Vergelijk C++ met \Cpp{}.
De \LaTeX{}-code van de vorige zin is: |Vergelijk C++ met \Cpp{}.|

Het commando |\proglink| kan gebruikt worden om een link te creëren naar de sourcecode van een programma op internet.
De URL van het directory waar de bij het betreffende document behorende programma's zich bevinden moet zijn gespecificeerd door het commando |\progurl| te definiëren.
In deze \MakeLowercase\documenttype{} is dat als volgt gedaan:

\lstinputfragment[
    style=lstyle,
    rangebeginprefix= \%>,
    rangeendprefix=\%<,
    linerange={progurl-progurl},
]{../main.tex}

De code |\proglink{lpfbodelog.m}| levert nu de volgende link op: \proglink{lpfbodelog.m}.

\section{Syntaxiskleuring}
\label{sec:syntaxiskleuring}
De syntaxiskleuring van de codefragmenten is afhankelijk van de gebruikte stijl.
Voor C-code en \Cpp{}-code wordt de kleurstelling van Code Composer Studio (dat gebaseerd is op Eclipse) gebruikt.

De code voor een programma dat alleen de watchdog timer van een MSP430 uitschakelt, ziet er als volgt uit:

\begin{lstfragment}[style=cstyle]
#include <msp430.h>

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer
    while (1)
    {
    }
    return 0;
}
\end{lstfragment}

Als je dit vergelijkt met de opmaak en kleurstelling van de code in Code Composer Studio, dan valt op dat de naam van de includefile \ccode{msp430.h} niet blauw gekleurd is zoals in CCS.
Dit kun je oplossen door het volgende optionele argumenten aan het commando voor de listing, zie \cref{sec:lstcommands}, mee te geven: |alsoletter={<>.},morekeywords={[2]{<msp430.h>}}|.

\begin{lstfragment}[style=cstyle,alsoletter={<>.},morekeywords={[2]{<msp430.h>}}]
#include <msp430.h>
\end{lstfragment}    