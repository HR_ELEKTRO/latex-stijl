# LaTeX-stijl Hogeschool Rotterdam opleiding Elektrotechniek #

In dit repository is de handleiding voor de LaTeX-stijl opgenomen die bij de opleiding Elektrotechniek van de Hogeschool Rotterdam gebruikt wordt.
De handleiding is uiteraard in de beschreven LaTeX-stijl geschreven.
Er zijn meerdere documenten beschikbaar die je als voorbeeld kunt gebruiken.
Deze documenten bevinden zich in diverse repositories waar vanuit de handleiding naar wordt verwezen.

Je kunt voordat je zelf documenten in deze stijl wilt gaan maken of voordat je deze stijl naar je eigen smaak gaat aanpassen het beste de hele handleiding eerst even doorlezen.
Je kunt de gecompileerde handleiding vinden in de [Downloads](https://bitbucket.org/HR_ELEKTRO/latex-stijl/downloads) van dit repository.

Een installatie- en gebruikshandleiding kun je vinden in de [Wiki](https://bitbucket.org/HR_ELEKTRO/latex-stijl/wiki/Home) van deze repository.

Op- en aanmerkingen zijn altijd welkom.
Maak een [issue](https://bitbucket.org/HR_ELEKTRO/latex-stijl/issues?status=new&status=open) aan of stuur een mail naar [Harry Broeders](mailto:j.z.m.broeders@hr.nl)
